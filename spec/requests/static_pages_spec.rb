require 'spec_helper'

describe "Static pages" do
  let(:base_title) {"Disco Melee"}
  describe "Home page" do

    it "should have the h1 'Disco Melee'" do
      visit '/static_pages/home'
      page.should have_selector('h1', :text=>'Disco Melee')
    end
    it "should have the base title" do
      visit '/static_pages/home'
      page.should have_selector('title',
                                :text => "#{base_title}")
    end
    it "should not have the custom page title"do
      visit '/static_pages/home'
      page.should_not have_selector('title', :text => '| Home')
    end
  end
  describe "About page" do

    it "should have the content 'About Us'" do
      visit '/static_pages/about'
      page.should have_content('About Us')
    end
    it "should have the right title" do
      visit '/static_pages/about'
      page.should have_selector('title',
                                :text => "#{base_title} | About Us")
    end

  end
  describe "Contact page" do

    it "should have the content 'Contact Us'" do
      visit '/static_pages/contact'
      page.should have_content('Contact Us')
    end
    it "should have the right title" do
      visit '/static_pages/contact'
      page.should have_selector('title',
                                :text => "#{base_title} | Contact Us")
    end
  end
  describe "News page" do

    it "should have the content 'News'" do
      visit '/static_pages/news'
      page.should have_content('News')
    end
    it "should have the right title" do
      visit '/static_pages/news'
      page.should have_selector('title',
                                :text => "#{base_title} | News")
    end
  end
end