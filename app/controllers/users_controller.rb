class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:success] = "Thank you for signing up!"
      redirect_to :root
    else
      flash[:error] = "The email already exists within our records"
      redirect_to :root
    end
  end
end
