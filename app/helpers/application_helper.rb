module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title)
    base_title = "Disco Melee"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end
  def google_font_link_tag(family)

    tag('link', {:rel => :stylesheet,
                 :type => Mime::CSS,
                 :href => "http://fonts.googleapis.com/css?family=#{family}"},
        false, false)

  end
end